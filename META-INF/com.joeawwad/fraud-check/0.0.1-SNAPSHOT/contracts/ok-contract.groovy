package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'PUT'
        url '/fraudcheck'
        body([
                "client.id": $(regex('[0-9]{1,}')),
                loanAmount : $(regex('[1-9]{4}'))
        ])
        headers {
            contentType('application/json')
        }
    }
    response {
        status OK()
        body([
                fraudCheckStatus   : "OK",
                "acceptance.reason": "Amount OK"
        ])
        headers {
            contentType('application/json')
        }
    }
}

