package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'PUT'
        url '/fraudcheck'
        body([
                "client.id": $(regex('[0-9]{1,}')),
                loanAmount : $(regex('[1-9]{6}'))
        ])
        headers {
            contentType('application/json')
        }
    }
    response {
        status OK()
        body([
                fraudCheckStatus  : "FRAUD",
                "rejection.reason": "Amount too high"
        ])
        headers {
            contentType('application/json')
        }
    }
}